import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'boxes/boxes.dart';
import 'models/notes_model.dart';

class ItemDialog extends StatefulWidget {
  const ItemDialog({
    super.key,
    this.itemnum,
    this.des,
    this.price,
    this.uom,
    this.itemKey,
    this.notesModel,
  });
  final String? itemnum;
  final String? des;
  final String? price;
  final String? uom;
  final int? itemKey;
  final NotesModel? notesModel;
  @override
  State<ItemDialog> createState() => _ItemDialogState();
}

class _ItemDialogState extends State<ItemDialog> {
  final _formKey = GlobalKey<FormState>();
  bool passToggle = true;
  final itemnumberController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();
  final uomController = TextEditingController();
  final List<String> listItem = [
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
    "Item 7",
    "Item 8",
    "Item 9",
    "Item 10",
  ];
  String selectedItems = "Item 1";

  List<Color> colors = [
    Color.fromARGB(255, 12, 0, 60),
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 173, 167, 0),
    Color.fromARGB(255, 3, 47, 83),
    Color.fromARGB(255, 141, 12, 2)
  ];

  Random random = Random(3);

  @override
  void initState() {
    super.initState();
    if (widget.itemnum!.isNotEmpty) {
      setState(() {
        itemnumberController.text = widget.itemnum!;
        descriptionController.text = widget.des!;
        priceController.text = widget.price!;
        selectedItems = widget.uom!;

        print("item : " + widget.itemnum!);
        print("des : " + widget.des!);
        print("price : " + widget.price!);
        print("uom : " + widget.uom!);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        widget.itemnum!.isEmpty ? 'Add Items' : 'Edit Items',
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Color.fromARGB(255, 114, 202, 0),
          fontWeight: FontWeight.bold,
        ),
      ),
      content: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                enabled: widget.itemnum!.isEmpty ? true : false,
                keyboardType: TextInputType.number,
                controller: itemnumberController,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r'\d*\.?\d+')),
                ],
                decoration: InputDecoration(
                  hintText: 'Enter ItemNumber',
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 93, 151, 18),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                validator: (use) {
                  if (use!.isEmpty) {
                    return 'Please enter Itemnumber';
                  } else if (use.length != 5 && use.length != 7) {
                    return "Value should be either 5 or 7";
                  }

                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                keyboardType: TextInputType.text,
                controller: descriptionController,
                decoration: InputDecoration(
                  hintText: 'Enter Description',
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 93, 151, 18),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                validator: (use) {
                  if (use!.isEmpty) {
                    return 'Please enter text';
                  } else if (descriptionController.text.length > 10) {
                    return "Text Length should be more than 10";
                  }

                  return null;
                },
              ),
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                controller: priceController,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                  TextInputFormatter.withFunction((oldValue, newValue) {
                    try {
                      final text = newValue.text;
                      if (text.isNotEmpty) double.parse(text);
                      return newValue;
                    } catch (e) {}
                    return oldValue;
                  }),
                ],
                decoration: InputDecoration(
                  hintText: 'Enter Price',
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Color.fromARGB(255, 93, 151, 18),
                    ),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide:
                        const BorderSide(color: Colors.black, width: 1.0),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                validator: (use) {
                  if (use!.isEmpty) {
                    return 'Please enter currency';
                  } else if (priceController.text.length > 6) {
                    return "Numbers in  decimal ";
                  }

                  return null;
                },
              ),
              SizedBox(
                height: 7.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: FormField<String>(
                  builder: (FormFieldState<String> state) {
                    return InputDecorator(
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.black, width: 1.0),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(
                          value: selectedItems,
                          isDense: true,
                          items: listItem.map<DropdownMenuItem<String>>((e) {
                            return DropdownMenuItem<String>(
                              value: e,
                              child: Text(e),
                            );
                          }).toList(),
                          onChanged: (String? val) {
                            setState(
                              () {
                                selectedItems = val!;
                              },
                            );
                          },
                        ),
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      actions: [
        TextButton(
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Text(
            'Cancel',
            style: TextStyle(color: Colors.white),
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
            ),
            backgroundColor: MaterialStateProperty.all(
              const Color.fromARGB(255, 114, 202, 0),
            ),
          ),
        ),
        TextButton(
          onPressed: () async {
            setState(() {
              if (_formKey.currentState!.validate()) {
                if (widget.itemnum!.isEmpty) {
                  final box = Boxes.getData();

                  List<NotesModel> templist = box.values
                      .cast<NotesModel>()
                      .where((element) =>
                          element.itemnumber == itemnumberController.text)
                      .toList();

                  if (templist.isEmpty) {
                    final data = NotesModel(
                      itemnumber: itemnumberController.text,
                      description: descriptionController.text,
                      price: priceController.text,
                      uom: selectedItems,
                    );

                    box.add(data);

                    itemnumberController.clear();
                    descriptionController.clear();
                    priceController.clear();

                    Navigator.pop(context);
                  } else {
                    final snackBar = SnackBar(
                      content: const Text('Item number should be unique'),
                      behavior: SnackBarBehavior.floating,
                      backgroundColor: Color.fromARGB(255, 93, 151, 18),
                      action: SnackBarAction(
                        label: 'Dismiss',
                        textColor: Colors.white,
                        onPressed: () {},
                      ),
                    );

                    ScaffoldMessenger.of(context).showSnackBar(snackBar);
                  }
                } else {
                  // final data = NotesModel(
                  //   itemnumber: itemnumberController.text,
                  //   description: descriptionController.text,
                  //   price: priceController.text,
                  //   uom: selectedItems,
                  // );

                  // final box = Boxes.getData();
                  // box.put(widget.itemKey, data);

                  widget.notesModel!.description = descriptionController.text;
                  widget.notesModel!.price = priceController.text;
                  widget.notesModel!.uom = selectedItems;

                  widget.notesModel!.save();

                  itemnumberController.clear();
                  descriptionController.clear();
                  priceController.clear();

                  Navigator.pop(context);
                }
              }
            });
          },
          child: Text(
            widget.itemnum!.isEmpty ? 'Add' : 'Edit',
            style: TextStyle(color: Colors.white),
          ),
          style: ButtonStyle(
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
            ),
            backgroundColor: MaterialStateProperty.all(
              const Color.fromARGB(255, 114, 202, 0),
            ),
          ),
        ),
      ],
    );
  }
}
