import 'package:flutter/material.dart';
import 'package:sarahebt/homepage.dart';
import 'package:sarahebt/names.dart';

class DashBoard extends StatefulWidget {
  const DashBoard({super.key});

  @override
  State<DashBoard> createState() => _DashBoardState();
}

class _DashBoardState extends State<DashBoard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("PRODUTS"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const HomePage())));
            },
          ),
        ],
      ),
      backgroundColor: const Color.fromARGB(255, 170, 182, 3),
      body: GridView.count(
        keyboardDismissBehavior: ScrollViewKeyboardDismissBehavior.onDrag,
        crossAxisCount: 3,
        children: [
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  content: const Text('Welcome Home !'),
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);

                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: ((context) => const Home())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.home_filled,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Home",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Welcome Account !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: ((context) => const Account())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.person,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Account",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Your Current Balance !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: ((context) => const Wallet())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.account_balance_rounded,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Wallet",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Your Collections !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: ((context) => const Collections())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.collections_rounded,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Collections",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Trun Your Privacy'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: ((context) => const Privacy())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.vpn_key,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Privacy",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Load Your Items !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: ((context) => Items())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.add_shopping_cart_outlined,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Items",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Your Current Loctaion !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: ((context) => const Location())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.add_location_alt_rounded,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "location",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Welcome to ThemeMode !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: ((context) => const DarkMode())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.dark_mode,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Darkmode",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Your Contacts !'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: ((context) => const Contacts())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.add_ic_call_rounded,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Contacts",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
          Card(
            margin: const EdgeInsets.all(20.0),
            elevation: 20.0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(50.0),
            ),
            child: InkWell(
              onTap: () {
                final snackBar = SnackBar(
                  behavior: SnackBarBehavior.floating,
                  backgroundColor: Color.fromARGB(255, 93, 151, 18),
                  content: const Text('Tune Your Settings'),
                  action: SnackBarAction(
                    label: 'Dismiss',
                    textColor: Colors.white,
                    onPressed: () {},
                  ),
                );

                ScaffoldMessenger.of(context).showSnackBar(snackBar);
                Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: ((context) => const Settings())));
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: const [
                  Icon(
                    Icons.settings,
                    color: Color.fromARGB(255, 93, 151, 18),
                    size: 30,
                  ),
                  Text(
                    "Settings",
                    style: TextStyle(
                        color: Color.fromARGB(255, 93, 151, 18),
                        fontWeight: FontWeight.bold,
                        fontSize: 15),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
