import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:sarahebt/models/notes_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sarahebt/add_item_dialogs.dart';
import 'package:sarahebt/configure.dart';
import 'package:sarahebt/homepage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var directory = await getApplicationDocumentsDirectory();
  Hive.init(directory.path);

  Hive.registerAdapter(NotesModelAdapter());
  await Hive.openBox<NotesModel>('notes');
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: "app",
      home: MyApp(),
      routes: {
        '/addItemDialog': (context) => const ItemDialog(),
      },
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  TextEditingController controller = TextEditingController();
  String name = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 247, 214, 114),
      body: Center(
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              ElevatedButton(
                onPressed: () async {
                  Future.delayed(
                    const Duration(seconds: 1),
                    () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => const HomePage(),
                        ),
                      );
                    },
                  );

                  showDialog(
                    context: context,
                    builder: (context) {
                      return const Center(
                        child: CircularProgressIndicator(
                          color: Colors.white,
                          strokeWidth: 2,
                          backgroundColor: Color.fromARGB(255, 114, 202, 0),
                        ),
                      );
                    },
                  );
                },
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(50.0)),
                  ),
                  backgroundColor: MaterialStateProperty.all(
                      const Color.fromARGB(255, 114, 202, 0)),
                ),
                child: const Text('Login'),
              ),
              ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    ),
                  ),
                  backgroundColor: MaterialStateProperty.all(
                    const Color.fromARGB(255, 114, 202, 0),
                  ),
                ),
                onPressed: () {
                  dialogPopUp('Confirmation');
                },
                child: const Text('Configure'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  dialogPopUp(String textMsg) {
    showDialog(
      context: context,
      builder: (context) {
        return StatefulBuilder(
          builder: (context, setStateForDialog) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              scrollable: true,
              title: Text(
                textMsg,
                textAlign: TextAlign.center,
              ),
              contentPadding:
                  const EdgeInsets.only(left: 20, right: 20, bottom: 20),
              content: Column(
                children: [
                  SizedBox(
                    height: 45.0,
                    child: TextField(
                      autocorrect: true,
                      keyboardType: TextInputType.text,
                      enableInteractiveSelection: false,
                      controller: controller,
                      decoration: InputDecoration(
                        counterText: "",
                        focusedBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.grey, width: 2.0),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: Colors.black, width: 1.0),
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                      ),
                    ),
                  ),
                  Visibility(
                    visible: name == '' ? false : true,
                    child: Column(
                      children: [
                        const SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          name,
                          style: const TextStyle(color: Colors.red),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 15.0,
                  ),
                  ElevatedButton(
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50.0)),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                        const Color.fromARGB(255, 114, 202, 0),
                      ),
                    ),
                    onPressed: () {
                      setStateForDialog(
                        () {
                          if (controller.text == "") {
                            name = 'Please a vaild name !';
                          } else if (controller.text == '0' ||
                              controller.text == '00') {
                            name = 'You cannot use 0 !';
                          } else {
                            name = "";
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return Configure(
                                dialogData: controller.text,
                              );
                            }));
                          }
                        },
                      );
                    },
                    child: const Text('Continue'),
                  ),
                  const SizedBox(
                    width: 15.0,
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: const Text(
                      'Cancel',
                      style: TextStyle(
                        color: Color.fromARGB(255, 114, 202, 0),
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}
