import 'package:flutter/material.dart';
import 'main.dart';

class Configure extends StatefulWidget {
  Configure({Key? key, this.dialogData}) : super(key: key);

  String? dialogData;
  @override
  State<Configure> createState() => _ConfigureState();
}

class _ConfigureState extends State<Configure> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        title: const Text('Configure'),
        centerTitle: true,
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const MyApp())));
            },
          ),
        ],
      ),
      backgroundColor: Color.fromARGB(255, 247, 214, 114),
      body: Center(
        child: Text(
          widget.dialogData!,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: 50,
            color: const Color.fromARGB(255, 114, 202, 0),
          ),
        ),
      ),
    );
  }
}
