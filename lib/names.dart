import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive_flutter/adapters.dart';
import 'package:sarahebt/boxes/boxes.dart';
import 'package:sarahebt/dasboard.dart';

import 'add_item_dialogs.dart';
import 'models/notes_model.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("HOME"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: const Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Account extends StatefulWidget {
  const Account({super.key});

  @override
  State<Account> createState() => _AccountState();
}

class _AccountState extends State<Account> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("ACCOUNT"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Wallet extends StatefulWidget {
  const Wallet({super.key});

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("WALLET"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Collections extends StatefulWidget {
  const Collections({super.key});

  @override
  State<Collections> createState() => _CollectionsState();
}

class _CollectionsState extends State<Collections> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("COLLECTIONS"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Privacy extends StatefulWidget {
  const Privacy({super.key});

  @override
  State<Privacy> createState() => _PrivacyState();
}

class _PrivacyState extends State<Privacy> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("PRIVACY"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Items extends StatefulWidget {
  const Items({
    Key? key,
  }) : super(key: key);

  @override
  State<Items> createState() => _ItemsState();
}

class _ItemsState extends State<Items> {
  final _formKey = GlobalKey<FormState>();
  bool passToggle = true;

  final itemnumberController = TextEditingController();
  final descriptionController = TextEditingController();
  final priceController = TextEditingController();
  final uomController = TextEditingController();
  final List<String> listItem = [
    "Item 1",
    "Item 2",
    "Item 3",
    "Item 4",
    "Item 5",
    "Item 6",
    "Item 7",
    "Item 8",
    "Item 8",
    "Item 9",
    "Item 10",
  ];
  String selectedItems = "Item 1";

  List<Color> colors = [
    Color.fromARGB(255, 12, 0, 60),
    Color.fromARGB(255, 0, 0, 0),
    Color.fromARGB(255, 173, 167, 0),
    Color.fromARGB(255, 3, 47, 83),
    Color.fromARGB(255, 141, 12, 2)
  ];

  Random random = Random(3);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: Text('Items'),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: const Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: ((context) => const DashBoard()),
                ),
              );
            },
          ),
        ],
      ),
      body: ValueListenableBuilder<Box<NotesModel>>(
        valueListenable: Boxes.getData().listenable(),
        builder: (context, box, _) {
          var data = box.values.toList().cast<NotesModel>();
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 12),
            child: ListView.builder(
              itemCount: box.length,
              reverse: true,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Card(
                    color: colors[random.nextInt(4)],
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 10),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(
                                data[index].itemnumber.toString(),
                                style: TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white),
                              ),
                              Spacer(),
                              InkWell(
                                onTap: () {
                                  delete(data[index]);
                                },
                                child: Icon(
                                  Icons.delete,
                                  color: Colors.white,
                                ),
                              ),
                              SizedBox(
                                width: 15,
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ItemDialog(
                                        itemnum: data[index].itemnumber,
                                        des: data[index].description,
                                        price: data[index].price,
                                        uom: data[index].uom,
                                        itemKey: data[index].key,
                                        notesModel: data[index],
                                      ),
                                    ),
                                  ).then(
                                    (value) {
                                      setState(() {});
                                    },
                                  );
                                  // _editDialog(
                                  //   data[index],
                                  //   data[index].itemnumber.toString(),
                                  //   data[index].description.toString(),
                                  //   data[index].price.toString(),
                                  //   data[index].uom.toString(),
                                  // );
                                },
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ),
                          ListTile(
                            title: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Text(
                                    data[index].description.toString(),
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                ),
                                Text(
                                  data[index].price.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                                Spacer(),
                                Text(
                                  data[index].uom.toString(),
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromARGB(255, 93, 151, 18),
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => const ItemDialog(
                itemnum: "",
              ),
            ),
          ).then(
            (value) {
              setState(() {});
            },
          );
        },
        child: Icon(Icons.add),
      ),
    );
  }

  void delete(NotesModel notesModel) async {
    await notesModel.delete();

    // final box = Boxes.getData();
    // box.delete(notesModel.key);
  }

  Future<void> _editDialog(NotesModel notesModel, String itemnumber,
      String description, String price, String uom) async {
    itemnumberController.text = itemnumber;
    descriptionController.text = description;
    priceController.text = price;
    selectedItems = uom;

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            'Change Items',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color.fromARGB(255, 114, 202, 0),
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    enabled: false,
                    keyboardType: TextInputType.number,
                    controller: itemnumberController,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'\d*\.?\d+')),
                    ],
                    decoration: InputDecoration(
                      hintText: 'Enter ItemNumber',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter Itemnumber';
                      } else if (use.length != 5 && use.length != 7) {
                        return "Value should be either 5 or 7";
                      }

                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.text,
                    controller: descriptionController,
                    decoration: InputDecoration(
                      hintText: 'Enter Description',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter text';
                      } else if (descriptionController.text.length < 10) {
                        return "Text Length should be more than 6 ";
                      }

                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    controller: priceController,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                      TextInputFormatter.withFunction((oldValue, newValue) {
                        try {
                          final text = newValue.text;
                          if (text.isNotEmpty) double.parse(text);
                          return newValue;
                        } catch (e) {}
                        return oldValue;
                      }),
                    ],
                    decoration: InputDecoration(
                      hintText: 'Enter Price',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter text';
                      } else if (descriptionController.text.length < 10) {
                        return "Text Length should be less than 10 ";
                      }

                      return null;
                    },
                  ),
                  SizedBox(
                    height: 7.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormField<String>(
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.black, width: 1.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              value: selectedItems,
                              isDense: true,
                              items:
                                  listItem.map<DropdownMenuItem<String>>((e) {
                                return DropdownMenuItem<String>(
                                  value: e,
                                  child: Text(e),
                                );
                              }).toList(),
                              onChanged: (String? val) {
                                setState(
                                  () {
                                    selectedItems = val!;
                                  },
                                );
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text(
                'Cancel',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(
                  const Color.fromARGB(255, 114, 202, 0),
                ),
              ),
            ),
            TextButton(
              onPressed: () async {
                notesModel.itemnumber = itemnumberController.text.toString();
                notesModel.description = descriptionController.text.toString();
                notesModel.price = priceController.text.toString();
                notesModel.uom = selectedItems;

                notesModel.save();
                descriptionController.clear();
                itemnumberController.clear();
                priceController.clear();

                // box.

                Navigator.pop(context);
              },
              child: Text(
                'Edit',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(
                  const Color.fromARGB(255, 114, 202, 0),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future<void> showMyDialog() async {
    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: const Text(
            'Add Items',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Color.fromARGB(255, 114, 202, 0),
              fontWeight: FontWeight.bold,
            ),
          ),
          content: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  TextFormField(
                    autocorrect: true,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r'\d*\.?\d+')),
                    ],
                    controller: itemnumberController,
                    decoration: InputDecoration(
                      hintText: 'Enter Itemnumber',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter Itemnumber';
                      } else if (use.length != 5 && use.length != 7) {
                        return "Value should be either 5 or 7";
                      }

                      return null;
                    },
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    autocorrect: true,
                    keyboardType: TextInputType.text,
                    controller: descriptionController,
                    decoration: InputDecoration(
                      hintText: 'Enter Description',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter text';
                      } else if (descriptionController.text.length < 10) {
                        return "Text Length should be less than 10 ";
                      }

                      return null;
                    },
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    autocorrect: true,
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      FilteringTextInputFormatter.allow(RegExp(r"[0-9.]")),
                      TextInputFormatter.withFunction((oldValue, newValue) {
                        try {
                          final text = newValue.text;
                          if (text.isNotEmpty) double.parse(text);
                          return newValue;
                        } catch (e) {}
                        return oldValue;
                      }),
                    ],
                    controller: priceController,
                    decoration: InputDecoration(
                      hintText: 'Enter Price',
                      focusedBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                          color: Color.fromARGB(255, 93, 151, 18),
                        ),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.black, width: 1.0),
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                    ),
                    validator: (use) {
                      if (use!.isEmpty) {
                        return 'Please enter currency';
                      } else if (priceController.text.length > 6) {
                        return "Numbers in  decimal ";
                      }

                      return null;
                    },
                  ),
                  SizedBox(
                    height: 7.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: FormField<String>(
                      builder: (FormFieldState<String> state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(
                                  color: Colors.black, width: 1.0),
                              borderRadius: BorderRadius.circular(20.0),
                            ),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton(
                              value: selectedItems,
                              isDense: true,
                              items:
                                  listItem.map<DropdownMenuItem<String>>((e) {
                                return DropdownMenuItem<String>(
                                  value: e,
                                  child: Text(e),
                                );
                              }).toList(),
                              onChanged: (String? val) {
                                setState(
                                  () {
                                    selectedItems = val!;
                                  },
                                );
                              },
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: const Text(
                'Cancel',
                style: TextStyle(color: Colors.white),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(
                  const Color.fromARGB(255, 114, 202, 0),
                ),
              ),
            ),
            TextButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  final data = NotesModel(
                    itemnumber: itemnumberController.text,
                    description: descriptionController.text,
                    price: priceController.text,
                    uom: selectedItems,
                  );

                  final box = Boxes.getData();
                  box.add(data);

                  itemnumberController.clear();
                  descriptionController.clear();
                  priceController.clear();

                  Navigator.pop(context);
                }
              },
              child: Text(
                'Add ',
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0),
                  ),
                ),
                backgroundColor: MaterialStateProperty.all(
                  const Color.fromARGB(255, 114, 202, 0),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}

//   TextEditingController list = TextEditingController();

//   TextEditingController list2 = TextEditingController();

//   TextEditingController list3 = TextEditingController();

//   TextEditingController list4 = TextEditingController();

//   List<User> userList = [];

//   List<String> listItem = ["Item 1", "Item 2", "Item 3", "Item 4", "Item 5"];
//   String selectedItems = "Item 1";
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         automaticallyImplyLeading: false,
//         backgroundColor: const Color.fromARGB(255, 255, 255, 255),
//         elevation: 20.0,
//         centerTitle: true,
//         title: const Text("ITEMS"),
//         titleTextStyle: const TextStyle(
//             color: Color.fromARGB(255, 93, 151, 18),
//             fontWeight: FontWeight.bold,
//             fontSize: 20,
//             fontStyle: FontStyle.italic),
//         leading: Container(
//           padding:
//               const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
//           child: Image.asset('assets/images/full.png'),
//         ),
//         actions: [
//           IconButton(
//             icon: const Icon(Icons.logout),
//             color: const Color.fromARGB(255, 93, 151, 18),
//             onPressed: () async {
//               Navigator.of(context).pushReplacement(
//                 MaterialPageRoute(
//                   builder: ((context) => const DashBoard()),
//                 ),
//               );
//             },
//           ),
//         ],
//       ),
//       floatingActionButton: getFloatingAction(context),
//       body: ListView.builder(
//         itemBuilder: (ctx, index) {
//           return InkWell(
//             onLongPress: () {
//               User userData = userList[index];

//               list.text = userData.itemnumber;
//               list2.text = userData.discription;
//               list3.text = userData.price;
//               selectedItems = userData.uom;

//               AlertDialog alertdialog = AlertDialog(
//                 title: const Text(
//                   "Add Items",
//                   textAlign: TextAlign.center,
//                   style: TextStyle(
//                     color: Color.fromARGB(255, 114, 202, 0),
//                     fontWeight: FontWeight.bold,
//                   ),
//                 ),
//                 content: SingleChildScrollView(
//                   child: Column(
//                     children: [
//                       SizedBox(
//                         height: 50,
//                         child: TextFormField(
//                           autocorrect: true,
//                           keyboardType: TextInputType.number,
//                           enableInteractiveSelection: false,
//                           controller: list,
//                           decoration: InputDecoration(
//                             hintText: "Item Number",
//                             counterText: "",
//                             focusedBorder: OutlineInputBorder(
//                               borderSide: const BorderSide(
//                                 color: Color.fromARGB(255, 93, 151, 18),
//                               ),
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                             enabledBorder: OutlineInputBorder(
//                               borderSide: const BorderSide(
//                                   color: Colors.black, width: 1.0),
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                           ),
//                           validator: (use) {
//                             if (use!.isEmpty) {
//                               return 'Please enter Itemnumber';
//                             } else if (use.length != 5 && use.length != 7) {
//                               return "Value should be either 5 or 7";
//                             }
//                           },
//                         ),
//                       ),
//                       SizedBox(
//                         height: 7.0,
//                       ),
//                       TextFormField(
//                         autocorrect: true,
//                         keyboardType: TextInputType.text,
//                         enableInteractiveSelection: false,
//                         controller: list2,
//                         decoration: InputDecoration(
//                           hintText: "Description",
//                           counterText: "",
//                           focusedBorder: OutlineInputBorder(
//                             borderSide: const BorderSide(
//                               color: Color.fromARGB(255, 93, 151, 18),
//                             ),
//                             borderRadius: BorderRadius.circular(20.0),
//                           ),
//                           enabledBorder: OutlineInputBorder(
//                             borderSide: const BorderSide(
//                                 color: Colors.black, width: 1.0),
//                             borderRadius: BorderRadius.circular(20.0),
//                           ),
//                         ),
//                         validator: (use) {
//                           if (use!.isEmpty) {
//                             return 'Please enter text';
//                           } else if (list.text.length > 6) {
//                             return "Text Length should be more than 6 ";
//                           }
//                         },
//                       ),
//                       SizedBox(
//                         height: 7.0,
//                       ),
//                       TextFormField(
//                           autocorrect: true,
//                           keyboardType: TextInputType.number,
//                           inputFormatters: [
//                             FilteringTextInputFormatter.allow(
//                                 RegExp(r"[0-9.]")),
//                             TextInputFormatter.withFunction(
//                                 (oldValue, newValue) {
//                               try {
//                                 final text = newValue.text;
//                                 if (text.isNotEmpty) double.parse(text);
//                                 return newValue;
//                               } catch (e) {}
//                               return oldValue;
//                             }),
//                           ],
//                           enableInteractiveSelection: false,
//                           controller: list3,
//                           decoration: InputDecoration(
//                             hintText: "Price",
//                             counterText: "",
//                             focusedBorder: OutlineInputBorder(
//                               borderSide: const BorderSide(
//                                 color: Color.fromARGB(255, 93, 151, 18),
//                               ),
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                             enabledBorder: OutlineInputBorder(
//                               borderSide: const BorderSide(
//                                   color: Colors.black, width: 1.0),
//                               borderRadius: BorderRadius.circular(20.0),
//                             ),
//                           ),
//                           validator: (use) {
//                             if (use!.isEmpty) {
//                               return 'Please enter currency';
//                             } else if (list.text.length > 6) {
//                               return "Numbers in  decimal ";
//                             }
//                           }),
//                       SizedBox(
//                         height: 7.0,
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.all(8.0),
//                         child: FormField<String>(
//                             builder: (FormFieldState<String> state) {
//                           return InputDecorator(
//                             decoration: InputDecoration(
//                               border: OutlineInputBorder(
//                                 borderRadius: BorderRadius.circular(30.0),
//                               ),
//                               enabledBorder: OutlineInputBorder(
//                                 borderSide: const BorderSide(
//                                     color: Colors.black, width: 1.0),
//                                 borderRadius: BorderRadius.circular(20.0),
//                               ),
//                             ),
//                             child: DropdownButtonHideUnderline(
//                               child: DropdownButton<String>(
//                                 value: selectedItems,
//                                 isDense: true,
//                                 items:
//                                     listItem.map<DropdownMenuItem<String>>((e) {
//                                   return DropdownMenuItem<String>(
//                                     value: e,
//                                     child: Text(e),
//                                   );
//                                 }).toList(),
//                                 onChanged: (String? val) {
//                                   setState(
//                                     () {
//                                       selectedItems = val!;
//                                     },
//                                   );
//                                 },
//                               ),
//                             ),
//                           );
//                         }),
//                       ),
//                       const SizedBox(
//                         height: 10.0,
//                       ),
//                       ElevatedButton(
//                         style: ButtonStyle(
//                           shape: MaterialStateProperty.all(
//                             RoundedRectangleBorder(
//                                 borderRadius: BorderRadius.circular(50.0)),
//                           ),
//                           backgroundColor: MaterialStateProperty.all(
//                             const Color.fromARGB(255, 114, 202, 0),
//                           ),
//                         ),
//                         onPressed: () {
//                           setState(
//                             () {
//                               Navigator.pop(context);

//                               userList[index].itemnumber = list.text;
//                               userList[index].discription = list2.text;
//                               userList[index].price = list3.text;
//                               userList[index].uom = selectedItems;

//                               // userData.itemnumber = list.text;

//                               // userList[index] = userData;
//                             },
//                           );
//                         },
//                         child: Text('Change'),
//                       ),
//                     ],
//                   ),
//                 ),
//               );
//               showDialog(
//                 context: context,
//                 barrierDismissible: true,
//                 builder: (BuildContext context) {
//                   return alertdialog;
//                 },
//               );
//             },
//             child: Card(
//               margin: const EdgeInsets.all(4),
//               color: const Color.fromARGB(255, 93, 151, 18),
//               elevation: 20,
//               child: ListTile(
//                 title: Row(
//                   children: [
//                     Expanded(
//                       child: Text(
//                         userList[index].itemnumber,
//                         style: const TextStyle(
//                             color: Colors.white,
//                             fontSize: 20,
//                             fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 30,
//                     ),
//                     Expanded(
//                       child: Text(
//                         userList[index].discription,
//                         style: const TextStyle(
//                           color: Colors.white,
//                           fontSize: 15,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 30,
//                     ),
//                     Expanded(
//                       child: Text(
//                         userList[index].price,
//                         style: const TextStyle(
//                           color: Colors.white,
//                           fontSize: 15,
//                         ),
//                       ),
//                     ),
//                     const SizedBox(
//                       width: 30,
//                     ),
//                     Expanded(
//                       child: Text(
//                         userList[index].uom,
//                         style: const TextStyle(
//                           color: Colors.white,
//                           fontSize: 18,
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//                 trailing: GestureDetector(
//                   child: const Icon(
//                     Icons.delete,
//                     color: Colors.white,
//                   ),
//                   onTap: () {
//                     setState(
//                       () {
//                         userList.removeAt(index);
//                       },
//                     );
//                   },
//                 ),
//               ),
//             ),
//           );
//         },
//         itemCount: userList.length,
//       ),
//     );
//   }

//   Widget getFloatingAction(BuildContext context) {
//     return FloatingActionButton(
//       backgroundColor: const Color.fromARGB(255, 114, 202, 0),
//       onPressed: () {
//         Navigator.push(
//           context,
//           MaterialPageRoute(
//             builder: (context) => const ItemDialog(),
//           ),
//         ).then(
//           (value) {
//             setState(
//               () {
//                 userList.add(
//                   User(
//                     value['itemNo'],
//                     value['desc'],
//                     value['price'],
//                     value['uom'],
//                   ),
//                 );
//               },
//             );
//           },
//         );
//       },
//       child: Icon(Icons.add),
//     );
//   }
// }

class Location extends StatefulWidget {
  const Location({super.key});

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("LOCATION"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class DarkMode extends StatefulWidget {
  const DarkMode({super.key});

  @override
  State<DarkMode> createState() => _DarkModeState();
}

class _DarkModeState extends State<DarkMode> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("DARK MODE"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Contacts extends StatefulWidget {
  const Contacts({super.key});

  @override
  State<Contacts> createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("CONTACT"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}

class Settings extends StatefulWidget {
  const Settings({super.key});

  @override
  State<Settings> createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: const Color.fromARGB(255, 255, 255, 255),
        elevation: 20.0,
        centerTitle: true,
        title: const Text("SETTINGS"),
        titleTextStyle: const TextStyle(
            color: Color.fromARGB(255, 93, 151, 18),
            fontWeight: FontWeight.bold,
            fontSize: 20,
            fontStyle: FontStyle.italic),
        leading: Container(
          padding:
              const EdgeInsetsDirectional.symmetric(horizontal: 1, vertical: 5),
          child: Image.asset('assets/images/full.png'),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            color: Color.fromARGB(255, 93, 151, 18),
            onPressed: () async {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: ((context) => const DashBoard())));
            },
          ),
        ],
      ),
    );
  }
}
